enum PointProjection {
    ISOMETRIC;
    CARTESIAN;
}

class Point extends Position<Float> {
    public var projection(default, null): PointProjection;

    public function new(x: Float, y: Float, projection: PointProjection = CARTESIAN) {
        super(x, y);
        this.projection = projection;
    }

    public function to2D(): Point {
        var tmp:Point = new Point(x, y, projection);
        if(projection == ISOMETRIC) {
            tmp.x = cast (2 * y + x) / 2;
            tmp.y = cast (2 * y - x) / 2;
            tmp.projection = CARTESIAN;
        }
		return tmp;
	}

	public function toIso(): Point {
        var tmp:Point = new Point(x, y, projection);
        if(projection == CARTESIAN) {
            tmp.x = x - y;
            tmp.y = cast (x + y) / 2;
            tmp.projection = ISOMETRIC;
        }
		return tmp;
	}
}