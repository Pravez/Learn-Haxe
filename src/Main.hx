import hxd.Key;
import hxd.Event.EventKind;
import hxd.Window;
import h2d.Camera;
import h3d.Vector;

class Main extends hxd.App {
	var cam:Camera;
	var player:Player;
	var text:h2d.Text;
	var map: MapGenerator;
	var pointer: Point;

	static function main() {
		hxd.Res.initEmbed();
		new Main();
	}

	override private function init() {
		super.init();

		pointer = new Point(.0, .0);

		cam = new h2d.Camera(s2d);
		map = new MapGenerator(cam);

		this.player = new Player(cam, 4);
		this.player.init(cam);

		var font:h2d.Font = hxd.res.DefaultFont.get();
		text = new h2d.Text(font);
		text.textAlign = Left;
		text.setPosition(10, 0);
		s2d.addChild(text);

		hxd.Window.getInstance().addEventTarget(onEvent);
		Window.getInstance().displayMode = DisplayMode.Fullscreen;
	}

	override function update(dt:Float) {
		player.update(dt);
		cam.viewX = player.x + player.size.x / 2;
		cam.viewY = player.y + player.size.y / 2;
		var iso = map.tilePosition(player.getPosition());
		map.activateTile(pointer);
		text.text = 'positionX: ${player.x}\npositionY: ${player.y}\ntileX: ${iso.x}\ntileY: ${iso.y}\npointer: ${pointer.toString()}';
	}

	function onEvent(event:hxd.Event) {
		trace(event.toString());
		var pt = cam.globalToLocal(new h2d.col.Point(event.relX, event.relY));
		pointer.update(pt.x, pt.y);
		if(event.kind == EventKind.EKeyDown) {
			switch(event.keyCode) {
				case Key.G: map.toggleGrid();
				case Key.T: map.toggleTiles();
			}
		}
	}
}

typedef TiledMapData = {layers:Array<{data:Array<Int>}>, tilewidth:Int, tileheight:Int, width:Int, height:Int};
