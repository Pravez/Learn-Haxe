import h3d.Vector;

class Utils {
	public static function isoTo2D(pt:Vector):Vector {
		var tempPt:Vector = new Vector(0, 0);
		tempPt.x = (2 * pt.y + pt.x) / 2;
		tempPt.y = (2 * pt.y - pt.x) / 2;
		return (tempPt);
	}

	public static function twoDToIso(pt:Vector):Vector {
		var tempPt:Vector = new Vector(0, 0);
		tempPt.x = pt.x - pt.y;
		tempPt.y = (pt.x + pt.y) / 2;
		return (tempPt);
	}
}