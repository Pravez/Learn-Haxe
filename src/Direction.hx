@:enum abstract Direction(Int) {
	public var Up = 1;
	public var UpLeft = 2;
	public var Left = 3;
	public var DownLeft = 4;
	public var Down = 5;
	public var DownRight = 6;
	public var Right = 7;
	public var TopRight = 8;
	public var TopLeft = 10;
	public var Top = 9;
	public var Idle = 0;
	public var None = -1;

	inline function new(v)
		this = v;

	public static function from(x:Int, y:Int):Direction {
		var x = x >= 1 ? 1 : x <= -1 ? -1 : 0;
		var y = y >= 1 ? 1 : y <= -1 ? -1 : 0;

		if (x == 1) {
			if (y == 1)
				return cast Direction.DownRight;
			else if (y == 0)
				return cast Direction.Right;
			else if (y == -1)
				return cast Direction.TopRight;
		} else if (x == 0) {
			if (y == 1)
				return cast Direction.Down;
			else if (y == 0)
				return cast Direction.Idle;
			else if (y == -1)
				return cast Direction.Top;
		} else if (x == -1) {
			if (y == 1)
				return cast Direction.DownLeft;
			else if (y == 0)
				return cast Direction.Left;
			else if (y == -1)
				return cast Direction.TopLeft;
		}
		return cast Direction.None;
	}
}