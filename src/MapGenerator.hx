import h2d.Layers;
import h3d.Vector;
import hxd.Res;

class MapGenerator extends h2d.Layers {
	static final lineColor = 0xAAAAAA;
	static final recColor = 0xFF0000;
	final tileImage:h2d.Tile;
	var lines: Array<h2d.Graphics>;
	var tiles: h2d.Layers;
	var grid: h2d.Layers;
	var activatedTiles: h2d.Layers;

	private static final isoTileSize = new Position(64, 64);

	public function new(?parent:h2d.Object) {
		super(parent);
		tileImage = Res.ground_tileset.toTile();

		init();
	}

	function init() {
		tiles = new h2d.Layers(this);
		grid = new h2d.Layers(this);
		activatedTiles = new h2d.Layers(this);
		lines = [];
		var group = new h2d.TileGroup(tileImage, tiles);

		var tw = 128;
		var th = 128;
		var mw = 100;
		var mh = 100;

		var perlin = new libnoise.generator.Perlin(1, 1, 1, 1, 42, libnoise.QualityMode.LOW);

		var tiles = [
			for (y in 0...Std.int(tileImage.height / 128))
				for (x in 0...Std.int(tileImage.width / 128))
					tileImage.sub(x * tw, y * th, tw, th)
		];

		for (y in -mh...mh) {
			for (x in -mw...mw) {
				var noise = perlin.getValue(x, y, 0.5);
				var tid = Math.ceil(Math.abs(noise * 3)) + 1;
				if (tid > 0) {
					var positions = new Point(x * isoTileSize.x, y * isoTileSize.y).toIso();
					group.add(positions.x, positions.y, tiles[tid - 1]);
				}
			}
		}

		for (y in -mh...mh+1) {
			var line = new h2d.Graphics(grid);
			var startPos = new Point(-mw*isoTileSize.x, y*isoTileSize.y).toIso();
			var endPos = new Point(mw*isoTileSize.x, y*isoTileSize.y).toIso();
			line.lineStyle(2, lineColor, 0.4);
			line.lineTo(64+startPos.x, startPos.y);
			line.lineTo(64+endPos.x, endPos.y);
			lines.push(line);
		}

		for (x in -mw...mw+1) {
			var line = new h2d.Graphics(grid);
			var startPos = new Point(x*isoTileSize.x, -mh*isoTileSize.y).toIso();
			var endPos = new Point(x*isoTileSize.x, mh*isoTileSize.y).toIso();
			line.lineStyle(2, lineColor, 0.4);
			line.lineTo(64+startPos.x, startPos.y);
			line.lineTo(64+endPos.x, endPos.y);
			lines.push(line);
		}
	}

	public function toggleGrid() {
		grid.visible = !grid.visible;
	}

	public function toggleTiles() {
		tiles.visible = !tiles.visible;
	}

	public function activateTile(position: Point) {
		activatedTiles.removeChildren();
		var rec = new h2d.Graphics(activatedTiles);
		var tile = tilePosition(position);
		var pt = new Point(tile.x*isoTileSize.x, tile.y*isoTileSize.y);
		var ptA = pt.toIso();
		var ptB = new Point(pt.x + isoTileSize.x, pt.y).toIso();
		var ptC = new Point(pt.x + isoTileSize.x, pt.y + isoTileSize.y).toIso();
		var ptD = new Point(pt.x, pt.y + isoTileSize.y).toIso();
		rec.lineStyle(2, recColor, 0.8);
		rec.setPosition(64+ptA.x, ptA.y);
		rec.lineTo(64+ptB.x, ptB.y);
		rec.lineTo(64+ptC.x, ptC.y);
		rec.lineTo(64+ptD.x, ptD.y);
		rec.lineTo(64+ptA.x, ptA.y);
	}

	public function tilePosition(point: Point): Position<Int> {
		var pt = point.projection == ISOMETRIC ? point.to2D() : point;
		var x = pt.x;
		var y = pt.y;
		return new Position(cast x/isoTileSize.x, cast y/isoTileSize.y);
	}
}
