import h2d.Anim;
import h2d.Tile;
import h3d.Vector;
import hxd.Key;

class Player extends h2d.Object {
	var spriteIndex:Int;

	var animation:Anim;
	var currentDirection:Direction;
	var isIdle:Bool;
	var animations:Map<Direction, Array<Tile>>;
	var idles:Map<Direction, Tile>;

	public var size(default, null):Vector;

	var dirText:h2d.Text;

	public function new(?parent:h2d.Object, spriteIndex:Int) {
		super(parent);

		this.spriteIndex = spriteIndex;
	}

	public function init(s2d:h2d.Object) {
		var charImage = hxd.Res.spaghetti_atlas.toTile();
		var charWidth = Math.floor(charImage.width / 24);
		var charHeight = Math.floor(charImage.height / 12);
		var charTiles = [
			for (y in spriteIndex*2...spriteIndex*2 + 2)
				for (x in 0...24)
					charImage.sub(x * charWidth, y * charHeight, charWidth, charHeight)
		];

		var idleXIndex = (spriteIndex * 8) % 24;
		var idleYIndex = charHeight * (10+Math.floor(spriteIndex/3)) + Math.ceil((spriteIndex * 8) / 24);
		var idleTiles = [
			for (x in idleXIndex...idleXIndex + 8)
				charImage.sub(x * charWidth, idleYIndex, charWidth, charHeight)
		];

		var rightAnim = charTiles.slice(0, 6);
		var topRightAnim = charTiles.slice(6, 12);
		var topAnim = charTiles.slice(12, 18);
		var topLeftAnim = charTiles.slice(18, 24);
		var leftAnim = charTiles.slice(24, 30);
		var bottomLeftAnim = charTiles.slice(30, 36);
		var bottomRightAnim = charTiles.slice(36, 42);
		var bottomAnim = charTiles.slice(42, 48);
		size = new Vector(rightAnim[0].width, rightAnim[0].height);

		animations = new Map();
		animations.set(Direction.from(1, 0), rightAnim);
		animations.set(Direction.from(1, -1), topRightAnim);
		animations.set(Direction.from(0, -1), topAnim);
		animations.set(Direction.from(-1, -1), topLeftAnim);
		animations.set(Direction.from(-1, 0), leftAnim);
		animations.set(Direction.from(-1, 1), bottomLeftAnim);
		animations.set(Direction.from(0, 1), bottomAnim);
		animations.set(Direction.from(1, 1), bottomRightAnim);

		idles = new Map();
		idles.set(Direction.from(1, 0), idleTiles[0]);
		idles.set(Direction.from(1, -1), idleTiles[1]);
		idles.set(Direction.from(0, -1), idleTiles[2]);
		idles.set(Direction.from(-1, -1), idleTiles[3]);
		idles.set(Direction.from(-1, 0), idleTiles[4]);
		idles.set(Direction.from(-1, 1), idleTiles[6]);
		idles.set(Direction.from(0, 1), idleTiles[7]);
		idles.set(Direction.from(1, 1), idleTiles[5]);

		animation = new h2d.Anim(bottomAnim, 10, this);
		animation.setScale(0.8);
		animation.play(bottomAnim);

		currentDirection = Direction.from(0, 1);

		this.setPosition(0, 0);
		this.animation.setPosition(0, 0);

		var font:h2d.Font = hxd.res.DefaultFont.get();
		dirText = new h2d.Text(font);
		dirText.textAlign = Center;
		dirText.setPosition(0, -10);
		this.addChild(dirText);
	}

	public function update(dt:Float) {
		var moveX = 0;
		var moveY = 0;
		if (Key.isDown(Key.D)) {
			moveX++;
		}
		if (Key.isDown(Key.Q)) {
			moveX--;
		}
		if (Key.isDown(Key.S)) {
			moveY++;
		}
		if (Key.isDown(Key.Z)) {
			moveY--;
		}

		if (moveX == 0 && moveY == 0) {
			if (!this.isIdle)
				this.isIdle = true;
		} else {
			if (this.isIdle)
				isIdle = false;

			moveX *= 300;
			moveY *= 300;

			if(moveX != 0 && moveY != 0) {
				moveX = cast moveX * 0.7;
				moveY = cast moveY * 0.7;
			} 

			this.setPosition(this.x + dt * moveX, this.y + dt * moveY);
		}

		var direction = Direction.from(moveX, moveY);
		if (currentDirection != direction) {
			animation.play(this.isIdle ? [idles.get(currentDirection)] : animations.get(direction));
			currentDirection = direction;
			dirText.text = '(${moveX}, ${moveY})';
		}
	}

	public function getPosition(): Point {
		return new Point(x, y);
	}
}