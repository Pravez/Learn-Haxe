class Position<T> {
    public var x(default, set): T;
    public var y(default, set): T;

    public function new(x: T, y: T) {
        this.x = x;
        this.y = y;
    }

    inline function set_x(value: T) return this.x = value;
    inline function set_y(value: T) return this.y = value;

    public inline function update(x: T, y: T) {
        this.x = x;
        this.y = y;
    }

    public function toString(): String {
        return '(${x}, ${y})';
    }
}